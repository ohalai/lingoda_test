#!/bin/sh

if [ ! -f /var/lib/demo/database.sqlite ]; then
  mv -f /var/www/demo/data/database.sqlite /var/lib/demo/
fi
rm -rf /var/www/demo/data /var/www/demo/var/log
ln -s /var/lib/demo /var/www/demo/data
ln -s /var/log/demo /var/www/demo/var/log
