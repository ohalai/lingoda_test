#!/bin/bash

sudo -u www-data \
  flock -x /var/lib/demo/migrate.lock \
  bin/console doctrine:migrations:migrate -n --allow-no-migration
