#!/bin/bash

chown www-data:www-data -R /var/lib/demo /var/log/demo
chmod -f 0660 /var/lib/demo/* /var/log/demo/*
exit 0
