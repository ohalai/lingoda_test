# Lingoda DevOps Test Case


## Task 1

### Requirements

I believe it's unnecessary to provide a detailed installation guide for Docker and Kubernetes in this context. 
Additionally, for a production environment, I would typically prepare a Helm chart incorporating ConfigMap, Ingress, 
and other essential configurations. However, I've omitted this step for brevity in the current discussion.

### Prepare Dockerfile

To meet Symfony's requirement of PHP 8.2+, I've opted to use the php:8.2-apache-bullseye image. 
This image comes preloaded with all the necessary extensions.

Performing `apt install` and `apt remove` within a single `RUN` command is aimed at minimizing the overall image size.

In addition, I've included a call to `phpunit` as part of the build process. 
This approach helps ensure the consistency of the built images.

Typically, I employ a separate image for the build process, excluding unnecessary components like Composer, Node.js, 
and PHPUnit post-build. 
For the runtime environment, I limit the image to only include Nginx + PHP (or Apache + PHP in this case), 
further reducing the overall image size.

[Dockerfile](task1/Dockerfile)

```bash
docker build -t task1:latest task1/
```

### Install kubernetes manifests

I've prepared simple [manifests](task1/demo.yml) with deployment, service and ingress.

Sure, better way is to use helm to simplify management, but it's not required in this task :)

So you just need to install this manifests and point `demo.symfony.com` to your cluster ingress IP and enjoy.

```bash
kubectl create -f task1/storage.yml
kubectl create -f task1/demo.yml

echo "$(minikube ip) demo.symfony.com" | sudo tee -a /etc/hosts
curl -vs http://demo.symfony.com
```


## Task 2

Best practice is to use initContainers for doing migrations, or more better, helm pre-install hook. But here we have all 
required files inside already persist image, so we can use it to simplify this task.

I've already prepared custom entrypoint to run scripts before starting application, so currently
I need only to add script for migrations.

So [here it is](task2/entrypoint-prepare/50_migrate.sh).

I've used `flock` to ensure that different containers will not run migrations at the same time.

Run commands below:

```bash
docker build -t task2:latest task2/
kubectl patch --patch-file task2/demo.yml deploy/symfony-demo-app
```


## Task 3

### Horisontal scaling

HPA makes your application auto scaled to required pods count. It can make decisions on CPU/memory and on additional 
custom metrics.

I think, RPS is most valuable metrics for web application, so I decided to implement it for our demo app.

RPS can be accessed as custom metric from prometheus stack. In short, you have to install prometheus, export metrics 
from your ingress, and instruct prometheus to collect that metrics. Also you need a `prometheus-adapter`, which makes 
this metrics available to Kubernetes. Here is [a HOWTO](https://kubernetes.github.io/ingress-nginx/user-guide/monitoring/)
install kubernetes-prometheus stack and here - [how to](https://blog.sighup.io/scale-workloads-with-ingress-traffic/) 
prepare some metrics to use.

Here is a [simple HPA](task3/hpa.yml) which should increase our pods count if each pod processes more that 10 rps.

```bash
kubectl create -f task3/hpa.yml
```

### The new way

I was prompted that there is a new way: [KEDA](https://keda.sh).

It's more simpler to add, doesn't need to configure prometheus-operator with its terrible configs, takes metrics from 
many different sources etc. So I decided to try this.

First of all, I removed previous HPA and prometheus-adapter:

```bash
kubectl delete hpa symfony-demo-app
helm uninstall adapter
```

Then installed keda:

```bash
helm repo add kedacore https://kedacore.github.io/charts
helm repo update
helm install keda kedacore/keda --namespace keda --create-namespace
```

Created [ScaledObject](task3/keda.yml) and installed it:

```bash
kubectl create -f task3/keda.yml
```

Now we can produce a load with any available tool and check that our keda hpa is working:

```bash
% kubectl get hpa
NAME                        REFERENCE                     TARGETS           MINPODS   MAXPODS   REPLICAS   AGE
keda-hpa-symfony-demo-app   Deployment/symfony-demo-app   99871m/50 (avg)   4         20        6          68s
```

Here we can see that deployment is overloaded (target rps 99.8 but requested 50).

And in about 10-15 seconds:

```bash
% kubectl get hpa
NAME                        REFERENCE                     TARGETS           MINPODS   MAXPODS   REPLICAS   AGE
keda-hpa-symfony-demo-app   Deployment/symfony-demo-app   49747m/50 (avg)   4         20        12         76s
```

HPA added additional replicas and target rps in under the threshold.

Sometime after, when there is no requests, replicas decreased to idle count, because there are no requests at all.

```bash
% kubectl get hpa
NAME                        REFERENCE                     TARGETS              MINPODS   MAXPODS   REPLICAS   AGE
keda-hpa-symfony-demo-app   Deployment/symfony-demo-app   <unknown>/50 (avg)   4         20        2          10m
```

### Not only pod count

Sure, I also should take a look at application architecture.

And the primary thing which prohibits huge loads is database. In this example, `sqlite` is used, but it purpose is not 
high-loaded environments.

I'd prefer to upgrade it to mysql or postgresql. In case of running it inside a kube, i'd add a VPA, only to monitor and 
propose best cpu/memory requests.

VPA is simple to configure, there are too much [manuals](https://github.com/kubernetes/autoscaler/tree/master/vertical-pod-autoscaler) 
in the net. So I won't copy-paste here.

