#!/bin/sh

set -e
for s in $(ls /entrypoint-prepare); do
  echo Running preparation script: ${s}
  /bin/sh /entrypoint-prepare/${s}
done
set +e

exec /usr/local/bin/docker-php-entrypoint "$@"
